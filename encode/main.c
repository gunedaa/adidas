#include"encode.h"
int main(int argc, char *argv[] )
{
    if(argc != 3){
		printf("The number of arguments supplied is incorrect\n");
		return -1;
	}
    encode(argv[1], argv[2]);
    return 0;
}
void encode(char *inputFileName, char *outputFileName)
{
    FILE *inputFile = fopen(inputFileName, "r");
    FILE *outputFile = fopen(outputFileName, "w");

    if (inputFile == NULL)
	{
		printf("%s\n", "Read file can not be opened.");
	}
	if (outputFile == NULL)
	{
		printf("%s\n", "Write file can not be opened.");
	}

    uint8_t readChar; //memory to store the read element
	fread(&readChar, 1, 1, inputFile);

    while (!feof(inputFile)) { //check if there is a read element
        printf("Encoding: %C\n", readChar);
		// split byte into msn and lsn
		uint8_t rawMsn = catchMsn(readChar);
		uint8_t rawLsn = catchLsn(readChar);

		// set parity
		uint8_t msnWithParity = assignParityBits(rawMsn);
		uint8_t lsnWithParity = assignParityBits(rawLsn);

		//to keep while loop working
        fread(&readChar, 1, 1, inputFile);

		// write to output file
		fwrite(&msnWithParity , 1 , 1 , outputFile);
		fwrite(&lsnWithParity , 1 , 1 , outputFile);

		// debug
		printf("Raw MSN: %d \n", rawMsn);
		printf("Raw LSN: %d \n", rawLsn);
		printf("Encoded MSN: %d \n", msnWithParity);
		printf("Encoded LSN: %d \n", lsnWithParity);

	}
	printf("\n");
	fclose(inputFile);
	fclose(outputFile);

}
uint8_t catchMsn(uint8_t inputByte)
{
	uint8_t maskForMsn = 0xf0; // mask to catch msn: 1111 0000
	inputByte &= maskForMsn;
	inputByte >>= 1; //shift one right to have a right calculation on parity bits
	return inputByte;
}
uint8_t catchLsn(uint8_t inputByte)
{
	// mask to catch lsn: 0000 1111
	uint8_t maskForLsn = 0x0f;
	inputByte &= maskForLsn; // will return lsn

	inputByte <<= 3; //place to the right position for parity
	return inputByte;
}
uint8_t assignParityBit(uint8_t inputByte, uint8_t maskForD1, uint8_t maskForD2, uint8_t maskForD3, uint8_t maskForParity) // sets parity bit for one circle
{
	int counter = 0;
	uint8_t outputByte = 0x00;

	if (inputByte & maskForD1)
	{
		counter++;
	}

	if (inputByte & maskForD2)
	{
		counter++;
	}

	if (inputByte & maskForD3)
	{
		counter++;
	}

	if (counter % 2 != 0) //if the sum of data bits is not even
	{
		outputByte |= maskForParity; //make it even by changing parity bit
	}
	return inputByte |= outputByte;
}
uint8_t assignParityBits(uint8_t inputByte) // sets parity bits for all circles
{
/*
    mask for p2 -> 0x04
    mask for p1 -> 0x02
    mask for p0 -> 0x01

    mask for d0 -> 0x08
    mask for d1 -> 0x10
    mask for d2 -> 0x20
    mask for d3 -> 0x40
*/
	uint8_t byteToBeCompleted;
	byteToBeCompleted = assignParityBit(inputByte, 0x08, 0x10, 0x40, 0x02); //p1
	byteToBeCompleted = assignParityBit(byteToBeCompleted, 0x08, 0x10, 0x20, 0x01); //p0
	byteToBeCompleted = assignParityBit(byteToBeCompleted, 0x20, 0x10, 0x40, 0x04); //p2
	return byteToBeCompleted;
}
