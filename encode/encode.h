#ifndef ENCODE_H_INCLUDED
#define ENCODE_H_INCLUDED
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

void encode(char *inputFileName, char *outputFileName);
uint8_t catchMsn(uint8_t inputByte);
uint8_t catchLsn(uint8_t inputByte);
uint8_t assignParityBit(uint8_t inputByte, uint8_t maskForD1, uint8_t maskForD2, uint8_t maskForD3, uint8_t maskForParity);
uint8_t assignParityBits(uint8_t inputByte);


#endif // ENCODE_H_INCLUDED
