#include"decode.h"
int main(int argc, char *argv[] )
{
    if(argc != 3){
		printf("The number of arguments supplied is incorrect\n");
		return -1;
	}
    decode(argv[1], argv[2]);

    return 0;
}

void decode(char *inputFileName, char *outputFileName)
{
	FILE *inputFile = fopen(inputFileName, "r");
	FILE *outputFile = fopen(outputFileName, "w");

	if (inputFile == NULL)
	{
		printf("%s\n", "Read file cannot be opened.");
	}
	if (outputFile == NULL)
	{
		printf("%s\n", "Write file cannot be opened.");
	}

	fseek(inputFile, 0, SEEK_END);
	int sizeOfFile = ftell(inputFile);
	fseek(inputFile, 0, SEEK_SET);

	uint8_t completeByte = 0x00;
	uint8_t readChar;

	for (size_t i = 0; i < sizeOfFile / 2; i++)
	{
	    fread(&readChar, 1, 1, inputFile);
		completeByte = captureMsn(repairCorrupted(readChar));

	    fread(&readChar, 1, 1, inputFile);
		completeByte |= captureLsn(repairCorrupted(readChar));

		fwrite(&completeByte , 1 , 1 , outputFile);
		printf("Decoded: %C\n", completeByte);
	}
	fclose(inputFile);
	fclose(outputFile);
	printf("\n");
}


uint8_t captureMsn(uint8_t inputByte)
{
	uint8_t outputByte = inputByte & 0x78; //0111 1000 to capture data bits only
	outputByte <<= 1; //shift one left for right position
	return outputByte;
}

uint8_t captureLsn(uint8_t inputByte)
{
	uint8_t outputByte = inputByte & 0x78;
	outputByte >>= 3;
	return outputByte;
}

bool isDestroyed(uint8_t inputByte, uint8_t parityCircleMask)
{
	uint8_t mask = 0x80; //1000 0000
	uint8_t byteToTest = inputByte & parityCircleMask;
	uint8_t counter = 0;

	for (size_t i = 0; i < 8; i++)
	{
		if (byteToTest & mask)
		{
			counter++;
		}
		mask >>= 1;
	}

	if ((counter % 2) != 0)
	{
		return true; // error
	}
	return false; // no error
}

uint8_t repairCorrupted(uint8_t inputByte)
{
	uint8_t p0 = 0x39; //0011 1001
	uint8_t p1 = 0x5A; //0101 1010
	uint8_t p2 = 0x74; //0111 0100

	uint8_t outputByte = inputByte;

	if (isDestroyed(inputByte, p0)) // if p0 is corrupt
	{
		if (isDestroyed(inputByte, p1)) // if p1 is corrupt
		{
			if (isDestroyed(inputByte, p2)) // if p2 is corrupt
			{
				outputByte = inputByte ^ 0x10; // flip d1
			}
			else
			{
				outputByte = inputByte ^ 0x08; // flip d0
			}
		}
		else
		{
			if (isDestroyed(inputByte, p2))
			{
				outputByte = inputByte ^ 0x20; // flip d2
			}
			else
			{
				outputByte = inputByte ^ 0x01; // flip p0
			}
		}
	}
	else
	{
		if (isDestroyed(inputByte, p1))
		{
			if (isDestroyed(inputByte, p2))
			{
				outputByte = inputByte ^ 0x40; // flip d3
			}
			else
			{
				outputByte = inputByte ^ 0x02; // flip p1
			}
		}
		else
		{
			if (isDestroyed(inputByte, p2))
			{
				outputByte = inputByte ^ 0x04;// flip p2
			}
		}
	}
	return outputByte;
}
