#ifndef DECODE_H_INCLUDED
#define DECODE_H_INCLUDED
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

typedef int bool;
#define true 1
#define false 0

void decode(char *inputFileName, char *outputFileName);
uint8_t captureMsn(uint8_t inputByte);
uint8_t captureLsn(uint8_t inputByte);
bool isDestroyed(uint8_t inputByte, uint8_t parityCircle);
uint8_t repairCorrupted(uint8_t inputByte);

#endif // !DECODE_H
