#ifndef CHANNEL_H_INCLUDED
#define CHANNEL_H_INCLUDED
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<time.h>

void corruptTransmission(char *inputFileName, char *outputFileName);
uint8_t capsizeBit(uint8_t inputByte);
int generateRandomNr();

#endif
