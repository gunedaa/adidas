#include"channel.h"
//to keep track of how often the bits are flipped
uint8_t isFlipped[8] = {0, 0, 0, 0, 0, 0, 0, 0}; //initialize all indexes to 0

int main(int argc, char *argv[] )
{
    srand(time(0));
    if(argc != 3){
		printf("The number of arguments supplied is incorrect\n");
		return -1;
	}
    corruptTransmission(argv[1], argv[2]);

    return 0;
}
void corruptTransmission(char *inputFileName, char *outputFileName)
{
	FILE *inputFile = fopen(inputFileName, "r");
	FILE *outputFile = fopen(outputFileName, "w");

	if (inputFile == NULL)
	{
		printf("%s\n", "Read file cannot be opened.");
	}
	if (outputFile == NULL)
	{
		printf("%s\n", "Write file cannot be opened.");
	}

	uint8_t readChar;
	fread(&readChar, 1, 1, inputFile);

	while (!feof(inputFile)) {
		printf("Flipping: %C\n", readChar);

		// mismanage transmission
		uint8_t destroyedBit = capsizeBit(readChar);

		fread(&readChar, 1, 1, inputFile);

		// write destroyed bit to file
		fwrite(&destroyedBit , 1 , 1 , outputFile);
	}
	printf("\n");
	//show how often the bits are flipped
	printf("x is flipped %d nr of times.\n", isFlipped[0]);
	printf("D3 is flipped %d nr of times.\n", isFlipped[1]);
	printf("D2 is flipped %d nr of times.\n", isFlipped[2]);
	printf("D1 is flipped %d nr of times.\n", isFlipped[3]);
	printf("D0 is flipped %d nr of times.\n", isFlipped[4]);
	printf("P2 is flipped %d nr of times.\n", isFlipped[5]);
	printf("P1 is flipped %d nr of times.\n", isFlipped[6]);
	printf("P0 is flipped %d nr of times.\n", isFlipped[7]);
	printf("\n");
	fclose(inputFile);
	fclose(outputFile);
}

uint8_t capsizeBit(uint8_t inputByte) {

	uint8_t xorMask = 0x80; // initialize mask 1000 0000
	int randomNr = generateRandomNr();
	xorMask >>= randomNr; // shift mask by random no.

    switch(randomNr){
        case 0:
            isFlipped[0]++; //increment the nr of times x is flipped
            break;
        case 1:
            isFlipped[1]++; //increment the nr of times d3 is flipped
            break;
        case 2:
            isFlipped[2]++; //increment the nr of times d2 is flipped
            break;
        case 3:
            isFlipped[3]++; //increment the nr of times d1 is flipped
            break;
        case 4:
            isFlipped[4]++; //increment the nr of times d0 is flipped
            break;
        case 5:
            isFlipped[5]++; //increment the nr of times p2 is flipped
            break;
        case 6:
            isFlipped[6]++; //increment the nr of times p1 is flipped
            break;
        case 7:
            isFlipped[7]++; //increment the nr of times p0 is flipped
            break;
        default:
                printf ("ERROR: invalid randomNr: %d\n", randomNr);
                break;

    }
	uint8_t capsizedBit = inputByte ^ xorMask; // use random mask to flip bit
	return capsizedBit;
}
int generateRandomNr()
{
	return rand() / (RAND_MAX / 8);
}
